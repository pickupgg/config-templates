FROM library/nginx:1-alpine

ENV WEB_ROOT="/usr/share/nginx/html"

# Copy in the game configuration templates.
COPY . "$WEB_ROOT/"
